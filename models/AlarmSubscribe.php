<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Будильник. Подписка на время
 * @property string $name
 * @property string $time
 * @property string $push_token Токен для отправки пуш уведомления
 */
class AlarmSubscribe extends ActiveRecord
{
    public static function tableName()
    {
        return 'alarm_subscribes';
    }

    public function rules()
    {
        return [
            [['name', 'time', 'push_token'], 'required'],
            [['name', 'time', 'push_token'], 'string'],
        ];
    }
}
