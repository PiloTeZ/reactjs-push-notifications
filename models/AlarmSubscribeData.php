<?php

namespace app\models;

use yii\base\Model;

class AlarmSubscribeData extends Model
{
    public $name;
    public $time;
    public $pushToken;

    public function rules()
    {
        return [
            [['name', 'time', 'pushToken'], 'required'],
            [['name', 'time', 'pushToken'], 'string'],
        ];
    }
}
