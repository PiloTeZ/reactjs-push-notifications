import PushService from "./PushService";

class AlarmService {
	constructor() {
		this.pushService = new PushService();
	}

	subscribe(name, time) {
		this.pushService
			.getAccess()
			.then(() => {
				return $.post('/alarm/subscribe', {
					'name': name,
					'time': time,
					'pushToken': this.pushService.getToken()
				});
			});
	}
}

export default AlarmService;
