var firebase = require('firebase/app');
require('firebase/messaging');

class PushService {
	constructor() {
		this.app = firebase.initializeApp({});

		this.messaging = this.app.messaging();
	}

	getAccess() {
		let context = this;
		return new Promise(function (resolve, reject) {
			Notification
				.requestPermission()
				.then((permission) => {
					if (permission === 'granted') {
						context.updateToken();
						resolve();
					} else {
						alert('Вы отказались от получения пуш уведомлений');
						reject();
					}
				})
				.catch(() => {
					reject();
				});
		});
	}

	updateToken() {
		this.messaging
			.getToken()
			.then((currentToken) => {
				if (currentToken) {
					this.saveToken(currentToken);
				} else {
					this.saveToken(null);
				}
			})
			.catch((err) => {
				this.saveToken(null);
			});
	}

	saveToken(token) {
		return $.cookie('push-token', token, {expires: 360});
	}

	getToken() {
		return $.cookie('push-token');
	}
}

export default PushService;
