import React from "react";
import AlarmService from "../services/AlarmService";

class AlarmForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			name: '',
			time: ''
		};
		this.timeSubscribeService = new AlarmService();

		this.handleChangeName = this.handleChangeName.bind(this);
		this.handleChangeTime = this.handleChangeTime.bind(this);
	}

	handleChangeName(event) {
		this.setState({name: event.target.value});
	}

	handleChangeTime(event) {
		this.setState({time: event.target.value});
	}

	handleSubmit(event) {
		event.preventDefault();

		if (!this.state.name || !this.state.time) {
			return;
		}

		this.timeSubscribeService.subscribe(this.state.name, this.state.time);
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit.bind(this)}>
				<div>
					Имя:
					<input type="text" value={this.state.name} onChange={this.handleChangeName}/>
				</div>
				<div>
					Время:
					<input type="time" value={this.state.time} onChange={this.handleChangeTime}/>
				</div>
				<input type="submit" value="Подписаться"/>
			</form>
		)

	}
}

export default AlarmForm;
