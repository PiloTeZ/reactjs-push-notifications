<?php

namespace components;

use app\models\AlarmSubscribe;
use DateTime;
use jobs\AlarmSendJob;
use yii\base\BaseObject;

/**
 * Отправить уведомления для всех подписок подходящих на текущее время
 */
class AlarmSendAll extends BaseObject
{
    /**
     * @throws \Exception
     */
    public function execute()
    {
        $currentDate = new DateTime();
        $currentTime = $currentDate->format('H:s');
        foreach (AlarmSubscribe::find()->batch() as $batch) {
            /** @var AlarmSubscribe $subscribe */
            foreach ($batch as $subscribe) {
                if ($subscribe->time == $currentTime) {
                    \Yii::$app->alarmQueue->push($this->getJobBySubscription($subscribe));
                }
            }
        }
    }

    /**
     * @param AlarmSubscribe $subscribe
     * @return AlarmSendJob
     */
    private function getJobBySubscription(AlarmSubscribe $subscribe)
    {
        $job = new AlarmSendJob();
        $job->name = $subscribe->name;
        $job->time = $subscribe->time;
        $job->pushToken = $subscribe->push_token;

        return $job;
    }
}
