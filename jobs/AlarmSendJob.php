<?php

namespace jobs;

use yii\queue\JobInterface;

/**
 * Отправка пуш уведомления будильника
 */
class AlarmSendJob implements JobInterface
{
    public $name;
    public $time;
    public $pushToken;

    public function execute($queue)
    {
        // ... обращение к API firebase
    }
}
