<?php

namespace app\controllers;

use app\models\AlarmSubscribeData;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

class AlarmController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'subscribe' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Подписаться на время
     * @throws \yii\db\Exception
     */
    public function actionSubscribe()
    {
        $data = new AlarmSubscribeData();
        $data->load(Yii::$app->request->post());

        Yii::$app->alarm->subscribe($data);
    }
}
