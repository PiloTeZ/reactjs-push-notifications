<?php

namespace services;

use app\models\AlarmSubscribe;
use app\models\AlarmSubscribeData;
use components\AlarmSendAll;
use yii\base\BaseObject;
use yii\db\Exception;

/**
 * Будильник
 */
class AlarmService extends BaseObject
{
    /**
     * Подписаться на время
     * @param AlarmSubscribeData $data
     * @throws Exception
     */
    public function subscribe(AlarmSubscribeData $data)
    {
        $subscribe = new AlarmSubscribe();
        $subscribe->name = $data->name;
        $subscribe->time = $data->time;
        $subscribe->push_token = $data->pushToken;
        if (!$subscribe->save()) {
            throw new Exception('Не удалось сохранить подписку');
        }
    }

    /**
     * Отправить все уведомления
     * @throws \Exception
     */
    public function sendAll()
    {
        (new AlarmSendAll)->execute();
    }
}
