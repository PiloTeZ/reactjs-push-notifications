<?php

use yii\db\Migration;

/**
 * Class m190922_161158_alarm_subscribes
 */
class m190922_161158_alarm_subscribes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('alarm_subscribes', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64),
            'time' => $this->string(5),
            'push_token' => $this->string(64),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190922_161158_alarm_subscribes cannot be reverted.\n";

        return false;
    }
}
