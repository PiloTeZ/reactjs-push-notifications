<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * Будильник
 */
class AlarmController extends Controller
{
    /**
     * Отправка уведомлений
     * @throws \Exception
     */
    public function actionSendNotifications()
    {
        Yii::$app->alarm->sendAll();
    }
}
