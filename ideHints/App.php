<?php
/**
 * Файл создан исключительно для IDE. Не используется нигде в коде.
 * Добавляет возможность документировать кастомные провайдеры, например Yii::$app->alert
 */

/**
 * @property \services\AlarmService $alarm
 * @property \yii\queue\Queue $alarmQueue
 */
abstract class App extends \yii\base\Application
{
}

class Yii
{
    /** @var App */
    static $app;
}
